#include "test_data.h"
#include <gtest/gtest.h>

/// Unit tests for the TestData accessor functions.   Retrieves the
/// "random" data set from the data/pl_example_test_data.json test data file.

namespace {

  using namespace PLExample_Test;

  /// Common set of tests that input mat is the "random" test data set
  void checkTestData( const cv::Mat &mat )
  {
    ASSERT_EQ( 4, mat.dims );

    const auto sz = mat.size;
    ASSERT_EQ( 60, sz[0] );
    ASSERT_EQ( 1, sz[1] );
    ASSERT_EQ( 1, sz[2] );
    ASSERT_EQ( 1, sz[3] );
  }

  /// Retrieve the test data as doubles.
  TEST(TestData, retrieveAsDoubles)
  {
    cv::Mat mat( testData<double>(TEST_DATA_RANDOM) );

    ASSERT_EQ( mat.type(), CV_64FC1 );
    checkTestData( mat );
  }

  /// Retrieve the test data as integers (32 bit signed)
  TEST(TestData, retrieveAsInts)
  {
    cv::Mat mat( testData<int>(TEST_DATA_RANDOM) );

    ASSERT_EQ( mat.type(), CV_32SC1 );
    checkTestData( mat );
  }

  /// Retrieve the test data as unsigned chars
  TEST(TestData, retrieveAsUnsignedChars)
  {
    cv::Mat mat( testData<unsigned char>(TEST_DATA_RANDOM) );

    ASSERT_EQ( mat.type(), CV_8UC1 );
    checkTestData( mat );
  }

  TEST( TestData, RetrieveDataWithAt )
  {
    const cv::Mat &mat( theInstance().at(TEST_DATA_RANDOM) );

    // The data is natively signed char..
    ASSERT_EQ( mat.type(), CV_8SC1 );
    checkTestData( mat );
  }



    ///==== Test loading the ones_xxxx arrays ====

  struct OnesTestData {
  public:

    OnesTestData( int t_, int row_, int col_, int channels_ )
      : t(t_), rows(row_), cols(col_), channels(channels_)
      {;}

    std::string key() const {
      char name[20];
      snprintf( name, 20, "ones_%1d%1d%1d%1d", t, rows, cols, channels );
      return std::string(name);
    }

    int t, rows, cols, channels;
  };

  std::vector<OnesTestData> MakeKeys() {
    std::vector<OnesTestData> vec;
    std::array<int,2> values = {1,4};

    for( auto const t : values ) {
      for( auto const row : values ) {
        for( auto const col : values ) {
          for( auto const chan : values ) {
            vec.push_back( OnesTestData(t,row,col,chan) );
          }
        }
      }
    }

    return vec;
  }



  TEST( TestData, LoadOnesMatrices )
  {
    std::vector<OnesTestData> keys( MakeKeys() );

    for( auto const key : keys  ) {
      cv::Mat td( testData<int>( key.key() ) );

      const auto sz( td.size );

      //std::cerr << key.key() << " " << sz[0] << " " << sz[1] << " " << sz[2] << " " << sz[3]  << std::endl;

      ASSERT_EQ( 4, td.dims ) << "For test data key " << key.key() << " dims=" << td.dims << " expected " << 3;
      ASSERT_EQ( sz[0], key.t ) << "For test data key " << key.key() << " t=" << sz[0] << " expected " << key.t;
      ASSERT_EQ( sz[1], key.rows ) << "For test data key " << key.key() << " rows=" << sz[1] << " expected " << key.rows;
      ASSERT_EQ( sz[2], key.cols ) << "For test data key " << key.key() << " cols=" << sz[2] << " expected " << key.cols;
      ASSERT_EQ( sz[3], key.channels ) << "For test data key " << key.key() << " channels=" << sz[3] << " expected " << key.channels;

      // TODO:  Check contents

      cv::Vec<int,4> at(0,0,0,0);

      for( at[0] = 0; at[0] < sz[0]; at[0]++ ) {
        for( at[1] = 0; at[1] < sz[1]; at[1]++ ) {
          for( at[2] = 0; at[2] < sz[2]; at[2]++ ) {
            for( at[3] = 0; at[3] < sz[3]; at[3]++ ) {
              ASSERT_EQ( 1, td.at<int>(at) ) << "Mismatch at " << at[0] << "," << at[1] << "," << at[2] << "," << at[3] << " ; " << td.at<int>(at) << " != 1";
            }
          }
        }
      }

    }

  }


#ifdef HAS_TEST_DATA
  /// The following two tests only work with if the test-data-20190611.tar.bz2
  /// test data file has been expanded __inside__ the data/ directory
  TEST(TestData, LoadJSONFromDirectory)
  {
    NumPlTest::TestData data( TEST_DATA_PATH"/test-data-20190611/" );

    const size_t numTestFiles = 7;

    // Should contain 7 data sets
    ASSERT_EQ( data.size(), numTestFiles );

    // Test the iterator
    size_t count = 0;
    for( auto const &entry : data ) {
      ++count;
    }
    ASSERT_EQ( count, numTestFiles );

  }

  TEST(TestData, LoadNEPIJSONFromFile)
  {
    cv::Mat data = NumPlTest::LoadJsonToMat( TEST_DATA_PATH"/test-data-20190611/THR_1_std_2019_04_29_212739.094.json" );

    ASSERT_FALSE( data.empty() );

  }
#endif

}
