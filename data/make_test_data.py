#!/usr/bin/env python

import numpy as np
import cv2
import scipy.stats as stats

import itertools

import json

def mat_to_json( mat, shape=None ):

    if not shape:
        shape = mat.shape

    while(len(shape) < 4):
        shape = shape + (1,)

    m = { 'samples': shape[0],
          'rows': shape[1],
          'cols': shape[2],
          'channels': shape[3],
          'dtype': str(mat.dtype),
          'data': mat.flatten().tolist() }

    return m


if __name__ == '__main__':

    # Dict to accumulate test data.  Serialized to file at end of main()
    test_data = {}

    ## Fixed random seed for debugging
    np.random.seed(0)

    ## Default test data size
    buffer_len = 60

    # ***** Generate the all-zeroes data set
    test_data['zeros'] = mat_to_json( np.zeros( (buffer_len), dtype=np.int8 ) )

    # ***** Generate a random data set
    test_data['random'] = mat_to_json( np.random.randint(0,2, size=(buffer_len), dtype=np.int8 ) )

    # Generate a set of AxBxCxD data sets where {A,B,C,D} are all combinations
    # of 1 and 4.   Each is filled with 1.0 and named "ones_ABCD"
    # Used to test unserialization of different combinations of matrix sizes
    for i in itertools.product( [1,4], repeat=4 ):
        key = "ones_" + ''.join([str(x) for x in i])
        test_data[key] = mat_to_json( np.ones( i, dtype=np.int8 ) )

    # ***** Generate other data sets here...


    ### Write test_data to file
    with open( "pl_example_test_data.json", 'w' ) as f:
      json.dump( test_data, f, indent=2 )
