# num_pl_core
This public repository contains source code and documentation for the Numurus Processing Library (PL) Core component of the Data Processing Pipeline. 

It should be of use to developers of PL subroutines and algorithm blocks. By deriving from the classes in this repo, you get a leg up on adherance to the API specification that all PL components must conform to.

# Building the code
## Prereqs
The code is designed to be compiled on Linux hosts for Linux targets. It was tested on a Ubuntu 16.04 build host.

The build system is CMake/Make (cmake > 2.8.3).

Doxygen is used for markup documentation.

OpenCV is required. The top-level CMakeLists.txt specifies the ROS Kinetic Kame bundled version of OpenCV. It is strongly advised that you install ROS on your build host and use the corresponding bundled version of OpenCV, since that is the version that will be linked in deployment to a Numurus Data Processing Pipeline Controller. Alternatively, you may change the line in CMakeLists.txt from
*set(OpenCV_DIR /opt/ros/kinetic/share/OpenCV-3.3.1-dev)*
to another install location at your own peril.

## Building the library
From the root folder of the repository, the following steps build the library
```console
$ cmake .
$ make
```

The resultant *lib/libnum_pl_core.a* is the static library to link into your own code.

## Building the documentation
```console
$ Doxygen
```
will generate the Doxygen output in the *docs* folder.

# Developing Custom Routines
The API is fully specified by *include/pl_routine.h*. The abstract base class, PLRoutine, should be extended through a concrete subclass, but only the public API serves as an actual requirement. Any instaniable class that provides the same public-access functions constitutes an eligible custom routine.



