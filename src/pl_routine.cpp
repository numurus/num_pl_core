#include <iostream>
#include <stdexcept>

#include "pl_routine.h"

// Uncomment or add to your make variables for debugging with default logger
//#define __DEBUG_PL 
namespace Numurus
{

PLRoutine::PLRoutine()
{
	log = std::bind(&PLRoutine::defaultLog, this, 
			        std::placeholders::_1, 
			        std::placeholders::_2);
}

PLRoutine::~PLRoutine()
{}

bool PLRoutine::configure(const PLRoutine::Config &config)
{
	// Not much bounds checking to do here
	if ((config.name_.size() == 0) || (config.description_.size() == 0))
	{
		log(PL_WARN, "Invalid blank strings in configuration");
		return false;
	}

	// Assignment operator is okay
	config_ = config;

	// Always increment the configure_count_ on success.
	++status_.configure_count_;
	
	status_.configured_ = true;
	return true;
}

void PLRoutine::defaultLog(PLMsgVerbosity v, const std::string msg)
{
	const std::string& name = (config_.name_.size() == 0)? 
		"Unnamed Routine" : config_.name_;
	switch(v)
	{
		case PLRoutine::PL_DEBUG:
#ifdef __DEBUG_PL
			cout << "DEBUG: " << name << "--" << msg << std::endl;
			break;
#endif
		case PLRoutine::PL_INFO:
			std::cout << "INFO: " << name << "--" << msg << std::endl;
			break;
		case PLRoutine::PL_WARN:
			std::cerr << "WARN: " << name << "--" << msg << std::endl;
			break;
		case PLRoutine::PL_ERROR:
			std::cerr << "ERR: " << name << "--" << msg << std::endl;
			break;
		case PLRoutine::PL_FATAL:
			std::cerr << "FATAL: " << name << "--" << msg << std::endl;
			throw std::runtime_error(msg);
			break;
		default:
			std::cerr << "BAD_VERBOSITY:" << name << "--" << msg << std::endl;
	}
}

std::map<std::string, int> PLRoutine::getIntParams()
{
    return config_.int_params;
}

std::map<std::string, float> PLRoutine::getFloatParams()
{
    return config_.float_params;
}

std::map<std::string, std::string> PLRoutine::getStringParams()
{
    return config_.string_params;
}

} // namespace

