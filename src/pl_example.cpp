#include <string>

#include "pl_example.h"

namespace Numurus
{

bool PLRoutinePassThru::initialize()
{
    return PLRoutine::initialize();
}

bool PLRoutinePassThru::configure(const PLRoutine::Config &config)
{
    /*
     * Ensure that needed parameters have been passed by the PM.
     */
    if (!config.float_params.count("example_param"))
        return false;

    return PLRoutine::configure(config);
}

bool PLRoutinePassThru::process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out)
{
    /*
     * Retrieve a parameter.
     */
    float __attribute__((unused)) example_param = config_.float_params["example_param"];

    /*
     * Process data_in to create data_out. Normally some processing would be
     * done here, but in this case we just copy the data.
     */
    *data_out = data_in->clone();

    /*
     * Assign a quality score to the output data. In this case we leave the
     * quality score assigned to the input data.
     */
    *quality_out = quality_in;

    return true;
}

}
